module GuiaEmpresarialHelper
  def company_images(company)
    images = []
    images << company.logo unless company.logo_file_name.blank?
    images << company.photo0 unless company.photo0_file_name.blank?
    images << company.photo1 unless company.photo1_file_name.blank?
    images << company.photo2 unless company.photo2_file_name.blank?
    images << company.photo3 unless company.photo3_file_name.blank?
    images << company.photo4 unless company.photo4_file_name.blank?
    images << company.photo5 unless company.photo5_file_name.blank?
    images
  end

  def company_categories
    @company_categories ||= CompanyCategory.order(:name).all.reject{|t| t.companies.where(city_id: current_city_id).count == 0}
  end
end
