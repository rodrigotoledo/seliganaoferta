module OrdersHelper
  def order_tr_class(order)
    if order.cancelado?
      'danger'
    elsif order.aprovado?
      'success'
    elsif !order.product_ad_orders.aprovado.blank?
      'warning'
    end
  end
end
