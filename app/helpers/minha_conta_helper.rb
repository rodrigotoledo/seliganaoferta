module MinhaContaHelper
  def company_contact(company)
    phones = [
      company.contact_phone, company.contact_mobile
    ].reject{|t| t.blank?}.to_sentence
    contacts =
    [
      company.email,
      company.site,
      "Responsável no anunciante: #{company.contact_name || '-'}",
      phones
    ].reject{|t| t.blank?}.join('<br />').html_safe
  end
end
