module CompaniesHelper
  def company_status(company)
    if company.full_active?
      'Ativa e visível no site'
    else
      'Inativa'
    end
  end
end
