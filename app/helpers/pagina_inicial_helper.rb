module PaginaInicialHelper
  def banner_url(advertisement)
    advertisement.is_a?(ProductAd) ? ofertas_detalhes_path(advertisement) : advertisement.url
  end
end
