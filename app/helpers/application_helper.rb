module ApplicationHelper

  def default_title
    "Rota Empresarial - Seja onde for sua empresa passa por aqui"
  end

  def default_host_to_email
    Rails.env == "development" ? 'http://localhost:3000' : 'http://rotaempresarial.rtoledo.inf.br'
  end

  def email_value_from_param(object)
    !params[:email].blank? ? params[:email] : object.email
  end

  def flash_messages
    html = []
    flash.each do |k,v|
      new_key = k

      if k.to_s == "alert"
        new_key = :danger
      elsif k.to_s == "notice"
        new_key = :success
      end
      html << content_tag(:div, v.html_safe, class: "alert alert-#{new_key}")
    end
    html.join.html_safe
  end

  def body_id
    if current_page?(root_path)
      "index"
    else
      if request.path.include?('ofertas/detalhes')
        "ofertas-disponiveis"
      elsif request.path.include?('empresa/') || request.path.include?('area-atuacao/empresa')
        "guia-empresarial"
      else
        request.path.gsub("/",'-')[1..-1]
      end
    end
  end

  def all_cities
    City.all
  end

  def banner_at_top
    @banner_at_top ||= begin
      banner = Banner.where(active: true).where("? BETWEEN start_at AND end_at", Time.now).first
      if banner && !admin_signed_in?
        banner.delay.update_attribute(:views, banner.views.to_i + 1)
      end
      banner
    end
  end

  def current_city
    City.find(current_city_id)
  end

  def product_ad_price(product_ad)
    if product_ad.oferta?
      number_to_currency(product_ad.special_price)
    else
      'Vale Cupom'
    end
  end

  def close_advertisement
    session[:show_advertisement] ||= true
  end
end
