module OfertasHelper
  def product_ad_interests(product_ad)
    count = product_ad.views.to_i + product_ad.product_ad_orders.count
    (content_tag(:span, count, class: "countdown-amount")+' Interesses por esta oferta').html_safe
  end

  def product_ad_images(product_ad)
    images = [ product_ad.image ] + product_ad.product_ad_images.map(&:image)
    images << product_ad.company.logo unless product_ad.company.logo_file_name.blank?
    images << product_ad.company.photo0 unless product_ad.company.photo0_file_name.blank?
    images
  end

  def product_ads_siblings(product_ad)
    @product_ads_siblings ||= begin
      if product_ad.product_ads.blank? && product_ad.product_ad.blank?
        nil
      else
        product_ads_siblings = []
        # is father
        unless product_ad.product_ads.blank?
          product_ads_siblings += product_ad.product_ads
        else
          product_ads_siblings << product_ad.product_ad
          product_ads_siblings += product_ad.product_ad.product_ads.where("id <> ?",product_ad.id)
        end
        product_ads_siblings.compact
      end
    end
  end
end
