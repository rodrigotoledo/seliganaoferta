class MinhaContaController < ApplicationController
  before_action :authenticate_client!
  skip_before_action :authenticate_client!, only: [:validar_email, :validar_cpf, :notificacoes_compra]
  skip_before_filter :verify_authenticity_token, only: :notificacoes_compra
  def index
    @approved_product_ad_orders = ProductAdOrder.actives.order_by_end_use.aprovado.
      where(client_id: current_client.id)
    @pending_product_ad_orders = ProductAdOrder.actives.pending.
      where(client_id: current_client.id)
  end

  def cupons
    @approved_product_ad_orders = ProductAdOrder.actives.order_by_end_use.aprovado.where(client_id: current_client.id)
    @pending_product_ad_orders = ProductAdOrder.actives.pending.where(client_id: current_client.id)
    @expired_product_ad_orders = ProductAdOrder.expireds.where(client_id: current_client.id)
  end

  def validar_email
    if client_signed_in?
      if Client.where(email: params[:client][:email]).where('id <> ?', current_client.id).exists?
        render text: "false"
      else
        render text: "true"
      end
    else
      if Client.where(email: params[:client][:email]).exists?
        render text: "false"
      else
        render text: "true"
      end
    end
  rescue
    render text: "false"
  end

  def validar_cpf
    if client_signed_in?
      if Client.where(cpf: params[:client][:cpf]).where('id <> ?', current_client.id).exists?
        render text: "false"
      else
        render text: "true"
      end
    else
      if Client.where(cpf: params[:client][:cpf]).exists?
        render text: "false"
      else
        render text: "true"
      end
    end
  rescue
    render text: "false"
  end

  def notificacoes_compra
    transaction = PagSeguro::Transaction.find_by_notification_code(params[:notificationCode])
    if transaction.errors.empty?
      order_transaction = OrderTransaction.where(order_id: transaction.reference, code: transaction.code).first
      # existe a transacao porem o pedido ja foi processado manualmente
      if order_transaction && !order_transaction.order.pendente?
        return render nothing: true, status: 200
      end

      # nao existe a transacao entao esta tentando criar uma para uma referencia valida
      unless order_transaction
        order             = Order.where(id: transaction.reference).first
        order_transaction = order.build_order_transaction if order
        order_transaction ||= OrderTransaction.new
      end

      # so atualiza se conseguiu criar uma transacao valida ou ja existia
      if order_transaction
        order_transaction.full_response   = transaction.to_json
        order_transaction.payment_status  = transaction.status.status.to_s
        order_transaction.payment_method  = transaction.payment_method.type.to_s
        order_transaction.gross_amount    = transaction.gross_amount.to_f
        order_transaction.discount_amount = transaction.discount_amount.to_f
        order_transaction.fee_amount      = transaction.fee_amount.to_f
        order_transaction.net_amount      = transaction.net_amount.to_f
        order_transaction.extra_amount    = transaction.extra_amount.to_f
        order_transaction.transaction_id  = transaction.code
        order_transaction.code            = transaction.code
        order_transaction.save
      end
    end

    render nothing: true, status: 200
  end
end
