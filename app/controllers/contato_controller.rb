class ContatoController < ApplicationController
  def index
  end

  def enviar
    if ContactMailer.new_contact(params[:contact]).deliver
      flash[:success] = 'Sua mensagem foi enviada com sucesso. Em breve entraremos em contato com a resposta'
    else
      flash[:error] = 'Sua mensagem não pode ser enviada pois os parâmetros estão incorretos.'
    end
    redirect_to contato_path
  end
end
