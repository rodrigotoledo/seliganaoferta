class PaginaInicialController < ApplicationController
  def index
    @companies = Company.actives.where(city_id: current_city_id).order("rand()").limit(12)
    @master_advertisements = Advertisement.actives.masters.order("rand()")
    @smaller_advertisements = Advertisement.actives.smaller.order("rand()")

    unless admin_signed_in?
      (@master_advertisements + @smaller_advertisements).each do |advertisement|
        advertisement.delay.update_attribute(:views, advertisement.views.to_i + 1)
      end
    end
  end
end
