class BuscaController < ApplicationController
  def index
    @companies = Company.actives.where(city_id: current_city_id).
      where("name like ? OR about like ?",
        "%#{params[:q]}%","%#{params[:q]}%")
  end
end
