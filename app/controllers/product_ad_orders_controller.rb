class ProductAdOrdersController < ApplicationController
  before_action :set_product_ad_order, only: [:show, :edit, :update, :destroy]

  # GET /product_ad_orders
  # GET /product_ad_orders.json
  def index
    @product_ad_orders = ProductAdOrder.all
  end

  # GET /product_ad_orders/1
  # GET /product_ad_orders/1.json
  def show
  end

  # GET /product_ad_orders/new
  def new
    @product_ad_order = ProductAdOrder.new
  end

  # GET /product_ad_orders/1/edit
  def edit
  end

  # POST /product_ad_orders
  # POST /product_ad_orders.json
  def create
    @product_ad_order = ProductAdOrder.new(product_ad_order_params)

    respond_to do |format|
      if @product_ad_order.save
        format.html { redirect_to @product_ad_order, notice: 'Product ad order was successfully created.' }
        format.json { render :show, status: :created, location: @product_ad_order }
      else
        format.html { render :new }
        format.json { render json: @product_ad_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_ad_orders/1
  # PATCH/PUT /product_ad_orders/1.json
  def update
    respond_to do |format|
      if @product_ad_order.update(product_ad_order_params)
        format.html { redirect_to @product_ad_order, notice: 'Product ad order was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_ad_order }
      else
        format.html { render :edit }
        format.json { render json: @product_ad_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_ad_orders/1
  # DELETE /product_ad_orders/1.json
  def destroy
    @product_ad_order.destroy
    respond_to do |format|
      format.html { redirect_to product_ad_orders_url, notice: 'Product ad order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_ad_order
      @product_ad_order = ProductAdOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_ad_order_params
      params.require(:product_ad_order).permit(:order_id, :client_id, :product_ad_id, :real_price, :special_price, :discount, :status, :annotations, :coupon, :terms, :start_at, :end_at, :start_use_at, :end_use_at, :product_type)
    end
end
