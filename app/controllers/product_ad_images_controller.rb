class ProductAdImagesController < AdminController
  before_action :set_product_ad_image, only: [:show, :edit, :update, :destroy]

  # GET /product_ad_images
  # GET /product_ad_images.json
  def index
    @product_ad_images = ProductAdImage.joins(:product_ad).order("created_at desc").all
  end

  # GET /product_ad_images/1
  # GET /product_ad_images/1.json
  def show
  end

  # GET /product_ad_images/new
  def new
    @product_ad_image = ProductAdImage.new
  end

  # GET /product_ad_images/1/edit
  def edit
  end

  # POST /product_ad_images
  # POST /product_ad_images.json
  def create
    @product_ad_image = ProductAdImage.new(product_ad_image_params)

    respond_to do |format|
      if @product_ad_image.save
        format.html { redirect_to product_ad_images_url, notice: 'Product ad image was successfully created.' }
        format.json { render :show, status: :created, location: @product_ad_image }
      else
        format.html { render :new }
        format.json { render json: @product_ad_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_ad_images/1
  # PATCH/PUT /product_ad_images/1.json
  def update
    respond_to do |format|
      if @product_ad_image.update(product_ad_image_params)
        format.html { redirect_to product_ad_images_url, notice: 'Product ad image was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_ad_image }
      else
        format.html { render :edit }
        format.json { render json: @product_ad_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_ad_images/1
  # DELETE /product_ad_images/1.json
  def destroy
    @product_ad_image.destroy
    respond_to do |format|
      format.html { redirect_to product_ad_images_url, notice: 'Product ad image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_ad_image
      @product_ad_image = ProductAdImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_ad_image_params
      params.require(:product_ad_image).permit(:product_ad_id, :image)
    end
end
