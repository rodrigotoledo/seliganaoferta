class SiteTermsController < AdminController
  before_action :set_site_term, only: [:show, :edit, :update, :destroy]

  # GET /site_terms
  # GET /site_terms.json
  def index
    @site_terms = SiteTerm.all
  end

  # GET /site_terms/1
  # GET /site_terms/1.json
  def show
  end

  # GET /site_terms/new
  def new
    @site_term = SiteTerm.new
  end

  # GET /site_terms/1/edit
  def edit
  end

  # POST /site_terms
  # POST /site_terms.json
  def create
    @site_term = SiteTerm.new(site_term_params)

    respond_to do |format|
      if @site_term.save
        format.html { redirect_to @site_term, notice: 'Site term was successfully created.' }
        format.json { render :show, status: :created, location: @site_term }
      else
        format.html { render :new }
        format.json { render json: @site_term.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /site_terms/1
  # PATCH/PUT /site_terms/1.json
  def update
    respond_to do |format|
      if @site_term.update(site_term_params)
        format.html { redirect_to @site_term, notice: 'Site term was successfully updated.' }
        format.json { render :show, status: :ok, location: @site_term }
      else
        format.html { render :edit }
        format.json { render json: @site_term.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /site_terms/1
  # DELETE /site_terms/1.json
  def destroy
    @site_term.destroy
    respond_to do |format|
      format.html { redirect_to site_terms_url, notice: 'Site term was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site_term
      @site_term = SiteTerm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_term_params
      params.require(:site_term).permit(:terms)
    end
end
