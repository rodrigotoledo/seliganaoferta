class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy, :cancel, :approve, :use_all, :dont_use_all, :use_coupon, :dont_use_coupon]

  def update_pending
    OrderTransaction.delay.update_pending!
    flash[:notice] = 'Os pedidos pendentes estão sendo atualizados'
    redirect_to :back
  end

  # GET /orders
  # GET /orders.json
  def index
    @orders = []
    @orders += Order.pendente.all
    @orders += Order.aprovado.all
    @orders += Order.cancelado.all
  end


  def approved
    @orders = Order.aprovado
    render action: :index
  end

  def pending
    @orders = Order.pendente
    render action: :index
  end

  def cancelled
    @orders = Order.cancelado
    render action: :index
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # get
  def approve
    @order.approve_all
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido aprovado com sucesso.' }
      format.json { head :no_content }
    end
  end

  # get
  def cancel
    @order.cancel_all
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido cancelado com sucesso.' }
      format.json { head :no_content }
    end
  end

  # get
  def use_all
    @order.use_all
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Cupons do pedido utilizados com sucesso.' }
      format.json { head :no_content }
    end
  end

  def dont_use_all
    @order.dont_use_all
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Cupons do pedido estão disponíveis para uso novamente.' }
      format.json { head :no_content }
    end
  end

  def use_coupon
    @order.product_ad_orders.find(params[:product_ad_order_id]).use_coupon
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Cupom do pedido utilizado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def dont_use_coupon
    @order.product_ad_orders.find(params[:product_ad_order_id]).dont_use_coupon
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Cupom do pedido está disponível para uso novamente.' }
      format.json { head :no_content }
    end
  end

  def approve_coupon
    @order.product_ad_orders.find(params[:product_ad_order_id]).approve_coupon
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Cupom do pedido aprovado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def cancel_coupon
    @order.product_ad_orders.find(params[:product_ad_order_id]).cancel_coupon
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Cupom do pedido cancelado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:client_id, :status, :payment_qty, :total, :amount_by_qty, :transaction_code)
    end
end
