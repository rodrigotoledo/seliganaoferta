class CarrinhoController < ApplicationController
  before_action :authenticate_client!, only: [:pagamento, :efetuar_pagamento, :processando]
  def adicionar
    if cart.add_at(params[:qty].to_i, params[:id])
      flash[:success] = 'Adicionado com sucesso'
    else
      flash[:error] = 'Produto está encerrado'
    end
    redirect_to carrinho_path
  end

  def diminuir
    if cart.sub_at(params[:id])
      flash[:success] = 'Quantidade diminuída com sucesso'
    else
      flash[:error] = 'Produto inexistente no seu carrinho de cupons'
    end
    redirect_to carrinho_path
  end

  def remover
    if cart.destroy_at(params[:id])
      flash[:success] = 'Produto removido do seu carrinho'
    else
      flash[:error] = 'Produto inexistente no seu carrinho de cupons'
    end
    redirect_to carrinho_path
  end

  def pagamento
    unless cart.valid?
      flash[:warning] = 'Você não possui nenhum item em seu carrinho'
      return redirect_to root_path
    end
  end

  def efetuar_pagamento
    unless cart.valid?
      flash[:warning] = 'Você não possui nenhum item em seu carrinho'
      return redirect_to root_path
    end

    @order = Order.create_from_cart(cart, current_client)
    if @order
      # ocorre quando todos os produtos sao vales
      if @order.aprovado?
        session[:product_ad_order] = {}
        OrderMailer.delay.new_order(@order.id)
        flash[:success] = 'Parabéns, seus cupons já estão disponíveis.'
        return redirect_to root_path
      else
        OrderMailer.delay.new_order(@order.id)
        session[:product_ad_order] = {}
        payment                    = PagSeguro::PaymentRequest.new
        payment.reference          = @order.id
        payment.notification_url   = notificacoes_compra_url
        payment.redirect_url       = carrinho_processando_url

        @order.product_ad_orders.pendente.each do |product_ad_order|
          payment.items << {
            id: product_ad_order.id,
            description: "#{product_ad_order.product_ad.id} - #{product_ad_order.product_ad.title}",
            amount: product_ad_order.special_price,
            weight: 0
          }
        end

        response = payment.register

        # Caso o processo de checkout tenha dado errado, lança uma exceção.
        # Assim, um serviço de rastreamento de exceções ou até mesmo a gem
        # exception_notification poderá notificar sobre o ocorrido.
        #
        # Se estiver tudo certo, redireciona o comprador para o PagSeguro.
        if response.errors.any?
          raise response.errors.join("\n")
        else
          return redirect_to response.url
        end
      end
    end
    redirect_to root_path
  end

  def processando
    transaction = PagSeguro::Transaction.find_by_code(params[:transaction_id])
    if transaction.errors.empty?
      order = Order.where(client_id: current_client.id, id: transaction.reference).first
      if order
        if order.aprovado?
          flash[:success] = 'Suas ofertas estão sendo processadas'
        else
          order_transaction                 = order.build_order_transaction
          order_transaction.full_response   = transaction.to_json
          order_transaction.payment_status  = transaction.status.status.to_s
          order_transaction.payment_method  = transaction.payment_method.type.to_s
          order_transaction.gross_amount    = transaction.gross_amount.to_f
          order_transaction.discount_amount = transaction.discount_amount.to_f
          order_transaction.fee_amount      = transaction.fee_amount.to_f
          order_transaction.net_amount      = transaction.net_amount.to_f
          order_transaction.extra_amount    = transaction.extra_amount.to_f
          order_transaction.transaction_id  = transaction.code
          order_transaction.code            = transaction.code
          if order_transaction.save
            session[:product_ad_order] = {}
            flash[:success] = 'Suas ofertas estão sendo processadas'
          else
            flash[:error] = 'Suas ofertas não foram salvas pois o sistema está sobrecarregado'
          end
        end
      end
    end
    redirect_to root_path
  end
end
