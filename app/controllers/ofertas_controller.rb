class OfertasController < ApplicationController
  def detalhes
    @product_ad = ProductAd.where(active: true).friendly.find(params[:id])
    @product_ad.touch_views
  rescue
    redirect_to root_path
  end

  def disponiveis
    @product_ads = ProductAd.actives.only_masters.where(companies: {city_id: current_city_id}).order("product_ads.end_at, product_ads.start_at")
  end

  def encerradas
    @product_ads = ProductAd.expireds.where(companies: {city_id: current_city_id}).order("product_ads.end_at, product_ads.start_at")
  end
end
