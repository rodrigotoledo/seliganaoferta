class CidadesController < ApplicationController
  def index
    @cities = City.all
  end

  def trocar
    city = City.where(id: params[:id]).first
    session[:current_city_id] = city.id if city
    redirect_to :back
  end
end
