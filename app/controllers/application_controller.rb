class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_city_id, :cart, :show_advertisement, :company_signed_in?, :using_mobile?

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_navigation

  def current_city_id
    session[:current_city_id] ||= City.first.id
  end

  def using_mobile?
    request.user_agent =~ /Mobile|webOS/
  end

  def cart
    @cart ||= begin
      session[:product_ad_order] ||= {}
      Cart.new(session[:product_ad_order])
    end
  end

  def show_advertisement
    session[:show_advertisement]
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email,
      :password,
      :password_confirmation,
      :name,
      :phone,
      :mobile,
      :cpf,
      :rg,
      :address,
      :address_city,
      :address_state,
      :address_neighborhood,
      :address_zipcode,
      :newsletter,
      :city_id
      ) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email,
      :password,
      :password_confirmation,
      :current_password,
      :name,
      :phone,
      :mobile,
      :cpf,
      :rg,
      :address,
      :address_city,
      :address_state,
      :address_neighborhood,
      :address_zipcode,
      :newsletter,
      :city_id
      ) }
  end

  def company_signed_in?
    client_signed_in? && current_client.company_id
  end

  def store_navigation
    AccessLog.create(url: request.original_url,
      client_id: (client_signed_in? ?  current_client.id : nil),
      ip: request.remote_ip)
  end
end
