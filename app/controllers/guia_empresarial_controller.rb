class GuiaEmpresarialController < ApplicationController
  def index
    @companies = Company.actives.where(city_id: current_city_id).order(:name)
  end

  def empresas_por_area_atuacao
    @company_category = CompanyCategory.friendly.find(params[:id])
    @companies = @company_category.companies.actives.where(city_id: current_city_id)
  end

  def empresa
    @company = Company.actives.friendly.find(params[:id])
    unless admin_signed_in?
      @company.delay.update_attribute(:views, @company.views.to_i + 1)
    end
  end
end
