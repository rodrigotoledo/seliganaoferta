class PaginasController < ApplicationController
  def index
    @page = Page.where(link: params[:link]).first
    redirect_to root_path unless @page
  end

  def regras_gerais
    @site_term = SiteTerm.last
  end
end
