class ProductAdsController < AdminController
  before_action :set_product_ad, only: [:show, :edit, :update, :destroy, :duplicate]

  # GET /product_ads
  # GET /product_ads.json
  def index
    @active_product_ads = ProductAd.actives.order('start_at, end_at')
    @expired_product_ads = ProductAd.where("product_ads.end_at < ? OR active = ?", Date.today, false).order('end_at, end_use_at')
  end

  # GET /product_ads/1
  # GET /product_ads/1.json
  def show
  end

  # GET /product_ads/new
  def new
    @product_ad = ProductAd.new
  end

  # GET /product_ads/1/edit
  def edit
  end

  # POST /product_ads
  # POST /product_ads.json
  def create
    @product_ad = ProductAd.new(product_ad_params)

    respond_to do |format|
      if @product_ad.save
        format.html { redirect_to product_ads_url, notice: 'Product ad was successfully created.' }
        format.json { render :show, status: :created, location: @product_ad }
      else
        format.html { render :new }
        format.json { render json: @product_ad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_ads/1
  # PATCH/PUT /product_ads/1.json
  def update
    respond_to do |format|
      if @product_ad.update(product_ad_params)
        format.html { redirect_to product_ads_url, notice: 'Product ad was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_ad }
      else
        format.html { render :edit }
        format.json { render json: @product_ad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_ads/1
  # DELETE /product_ads/1.json
  def destroy
    @product_ad.destroy
    respond_to do |format|
      format.html { redirect_to product_ads_url, notice: 'Product ad was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def duplicate
    original_product_ad = @product_ad.product_ad.blank? ? @product_ad : @product_ad.product_ad

    new_product_ad = original_product_ad.dup
    new_product_ad.product_ad_id = original_product_ad.id
    new_product_ad.image = File.open(original_product_ad.image.path)
    new_product_ad.save
    original_product_ad.product_ad_images.each do |product_ad_image|
      new_product_ad_image = product_ad_image.dup
      new_product_ad_image.image = File.open(product_ad_image.image.path)
      new_product_ad_image.product_ad_id = new_product_ad.id
      new_product_ad_image.save
    end
    redirect_to edit_product_ad_path(new_product_ad)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_ad
      @product_ad = ProductAd.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_ad_params
      params.require(:product_ad).permit(:company_id, :product_ad_id, :title, :image, :description, :terms, :start_at, :end_at, :start_use_at, :end_use_at, :real_price, :special_price, :transfer_price, :minimum_per_client, :maximum_per_client, :views, :product_type, :is_hostel, :use_in_holiday, :active)
    end
end
