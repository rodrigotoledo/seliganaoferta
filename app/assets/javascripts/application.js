// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require ./site/app
//= require ./site/nivo
//= require ./site/source/bootstrap.min
//= require ./site/source/mainscript
//= require ./site/source/jquery.scrollpane
//= require ./site/source/jquery.uniform
//= require ./site/source/plugins
//= require ./site/source/footable
//= require ./site/source/jquery.mousewheel
//= require ./site/source/jquery.carouFredSel-6.2.1
//= require ./site/source/jquery.touchSwipe.min
//= require ./site/source/jquery.validate
//= require ./site/source/jquery.validate.messages_pt_BR
//= require ./site/source/jquery.fancybox
//= require ./site/source/jquery.fancybox.pack
//= require ./site/source/jquery.mask.min
//= require ./site/source/jquery.dataTables.min
//= require ./site/jquery.tinymce.js
//= require ./site/jquery.plugin.min.js
//= require ./site/jquery.countdown.min.js
//= require ./site/seliganaoferta
