$(document).on("page:change ready", function(){
  $('#btnCloseNavFooter').click(function(){
    $('.navbar-fixed-bottom').remove()
  })

  $('#buy_block .btn_add_cart').each(function(){
    $(this).click(function(e){
      e.preventDefault()
      $('#buy_block').submit()
    })
  });
  $('#order_steps li').css('width', ($('#order_steps').width() / $('#order_steps li').size())+'px')
  $('.list-order-step li').last().addClass ('last');
  $('#btnSearch').click(function(e){
    e.preventDefault()
    $('form#searchbox').submit()
  })


  $('#client_cpf').mask('000.000.000-00', {reverse: true});
  $('#client_address_zipcode').mask('00000000', {reverse: true});
  $('table.dt').dataTable()
  $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
  $('.countdown').each(function(){
    var end_at = $(this).data('end-at').split('-')
    console.log(new Date(end_at[0],parseInt(end_at[1]) - 1,end_at[2]))
    $(this).countdown({until: new Date(end_at[0],parseInt(end_at[1]) - 1,end_at[2]), format: 'dHM'})
  })
})


jQuery.validator.addMethod("cpf", function(value, element) {
   value = jQuery.trim(value);
  
  value = value.replace('.','');
  value = value.replace('.','');
  cpf = value.replace('-','');
  while(cpf.length < 11) cpf = "0"+ cpf;
  var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
  var a = [];
  var b = new Number;
  var c = 11;
  for (i=0; i<11; i++){
    a[i] = cpf.charAt(i);
    if (i < 9) b += (a[i] * --c);
  }
  if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
  b = 0;
  c = 11;
  for (y=0; y<10; y++) b += (a[y] * c--);
  if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
  
  var retorno = true;
  if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;
  
  return this.optional(element) || retorno;

}, "Informe um CPF válido.");
