class Company < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :city
  has_many :product_ads
  has_many :product_ad_orders, through: :product_ads
  has_many :company_categorizations
  has_many :company_categories, through: :company_categorizations
  has_one :client

  validates_presence_of :name, :state, :city_id, :email, :about

  has_attached_file :photo0, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo1, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo2, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo3, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo4, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo5, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :logo, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  do_not_validate_attachment_file_type :photo0
  do_not_validate_attachment_file_type :photo1
  do_not_validate_attachment_file_type :photo2
  do_not_validate_attachment_file_type :photo3
  do_not_validate_attachment_file_type :photo4
  do_not_validate_attachment_file_type :photo5
  do_not_validate_attachment_file_type :logo

  def self.actives
    where(active: true).where("? BETWEEN companies.start_at AND companies.end_at", Date.today)
  end

  def self.inactives
    where(active: false).where("? NOT BETWEEN companies.start_at AND companies.end_at", Date.today)
  end

  def full_active?
    active && Date.today >= start_at && Date.today <= end_at
  end

  def company_videos
    videos = []
    4.times do |t|
      videos << read_attribute("video#{t}".to_sym)
    end
    videos.reject{|t| t.blank? }
  end
end
