class CompanyCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :company_categorizations
  has_many :companies, through: :company_categorizations
end
