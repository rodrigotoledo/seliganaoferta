class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable,
    :validatable#, :registerable
  ADMIN_EMAILS = ['rodrigo.toledo@rotaempresarial.com.br',
    'rafael.alves@rotaempresarial.com.br',
  ]
end
