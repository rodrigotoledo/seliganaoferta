class City < ActiveRecord::Base
  has_many :companies
  has_many :product_ads, through: :companies

  def to_param
    [id,name.gsub(' ','-')].join('-')
  end
end
