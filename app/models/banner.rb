class Banner < ActiveRecord::Base
  has_attached_file :image, styles: {thumb: '76x76#', big: '600x114'}
  do_not_validate_attachment_file_type :image
end
