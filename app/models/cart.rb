class Cart
  attr_accessor :product_ad_orders

  def initialize(product_ad_orders = {})
    @product_ad_orders = product_ad_orders
  end

  def add_at(qty, id)
    qty = 1 if qty <= 0

    product_ad = ProductAd.actives.friendly.find(id)
    if product_ad
      # e a quantidade preferida é maior que o que pode ter por cliente, coloca o máximo por cliente
      qty = product_ad.maximum_per_client if qty > product_ad.maximum_per_client

      # quando nao existe na sessao
      if @product_ad_orders.has_key?(product_ad.id.to_s)
        # soma o que ja existe no carrinho com o que se pretende adicionar
        total_quantity = qty_at(product_ad) + qty
        if total_quantity > product_ad.maximum_per_client
          qty = product_ad.maximum_per_client
        elsif total_quantity < product_ad.minimum_per_client
          qty = product_ad.minimum_per_client
        else
          qty = total_quantity
        end
      else
        qty = product_ad.minimum_per_client if qty < product_ad.minimum_per_client
      end
      product_ad.touch_views

      @product_ad_orders[product_ad.id.to_s] = qty
      true
    else
      false
    end
  end

  def sub_at(id)
    product_ad = ProductAd.actives.friendly.find(id)
    if product_ad
      qty = -1
      if @product_ad_orders.has_key?(product_ad.id.to_s)
        total_quantity = qty_at(product_ad) + qty
        if total_quantity < product_ad.minimum_per_client
          qty = product_ad.minimum_per_client
        else
          qty = total_quantity
        end
        @product_ad_orders[product_ad.id.to_s] = qty
        return true
      end
    end
    false
  end

  def destroy_at(id)
    product_ad = ProductAd.actives.friendly.find(id)
    @product_ad_orders.except!(product_ad.id.to_s)
  end

  def valid?
    !product_ads.blank?
  end

  def qty_at(product_ad)
    @product_ad_orders[product_ad.id.to_s] || 0
  end

  def total_at(product_ad)
    return 0 if product_ad.vale?
    product_ad.special_price * qty_at(product_ad)
  end

  def product_ads
    @product_ads ||= ProductAd.actives.find(product_ad_orders.keys)
  end

  def total
    @total ||= begin
      product_ads.sum{|product_ad| total_at(product_ad) }
    end
  end

  def total_qty
    @product_ad_orders.values.sum
  end
end
