class CompanyCategorization < ActiveRecord::Base
  belongs_to :company
  belongs_to :company_category
end
