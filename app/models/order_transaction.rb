class OrderTransaction < ActiveRecord::Base
  belongs_to :order
  after_commit :touch_order, :approve_order, :cancel_order

  def self.pending
    where.not(order_id: nil).where("payment_status NOT IN(?)",["paid", "available", "refunded", "cancelled"])
  end

  def self.update_pending!
    OrderTransaction.pending.each do |order_transaction|
      if order_transaction.order.aprovado?
        order_transaction.payment_status = 'paid'
        order_transaction.payment_method = 'Aprovado Manualmente'
        order_transaction.save
      elsif order_transaction.order.cancelado?
        order_transaction.payment_status = 'cancelled'
        order_transaction.payment_method = 'Cancelado Manualmente'
        order_transaction.save
      else
        transaction = PagSeguro::Transaction.find_by_code(order_transaction.code)
        if transaction.errors.empty?
          order_transaction.full_response   = transaction.to_json
          order_transaction.payment_status  = transaction.status.status.to_s
          order_transaction.payment_method  = transaction.payment_method.type.to_s
          order_transaction.gross_amount    = transaction.gross_amount.to_f
          order_transaction.discount_amount = transaction.discount_amount.to_f
          order_transaction.fee_amount      = transaction.fee_amount.to_f
          order_transaction.net_amount      = transaction.net_amount.to_f
          order_transaction.extra_amount    = transaction.extra_amount.to_f
          order_transaction.save
        end
      end
    end
  end

  def paid?
    ["paid", "available"].include?(payment_status)
  end

  def cancelled?
    ["refunded", "cancelled"].include?(payment_status)
  end

  def approve_order
    if order && paid? && !order.aprovado?
      order.approve_all
    end
    true
  end

  def cancel_order
    if order && cancelled? && !order.cancelado?
      order.cancel_all
    end
    true
  end

  def touch_order
    order.touch if order
    true
  end
end
