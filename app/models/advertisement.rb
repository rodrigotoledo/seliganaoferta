class Advertisement < ActiveRecord::Base
  belongs_to :city

  has_attached_file :image, styles: {thumb: '76x76#', master: '770x372#', small: '370x178#'}
  do_not_validate_attachment_file_type :image

  def self.actives
    where(active: true).where("? BETWEEN start_at AND end_at", Date.today)
  end

  def self.masters
    where(master: true).limit(3)
  end

  def self.smaller
    where(small: true).limit(2)
  end
end
