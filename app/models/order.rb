class Order < ActiveRecord::Base
  enum status: [ :pendente, :aprovado, :cancelado ]
  belongs_to :client
  has_many :product_ad_orders, dependent: :destroy
  has_one :order_transaction

  def name
    "#{id} - #{client.name} (#{client.email})"
  end

  def self.pending
    joins(:order_transaction).pendente
  end

  def self.create_from_cart(cart, current_client)
    order = Order.new
    order.client_id = current_client.id
    order.total = cart.total
    order.status = 0
    cart.product_ads.each do |product_ad|
      1.upto(cart.qty_at(product_ad)) do
        order.product_ad_orders.build(
          client_id: current_client.id,
          product_ad_id: product_ad.id,
          transfer_price: product_ad.transfer_price,
          real_price: product_ad.real_price,
          special_price: product_ad.special_price,
          discount: product_ad.discount,
          terms: product_ad.terms,
          start_at: product_ad.start_at,
          end_at: product_ad.end_at,
          start_use_at: product_ad.start_use_at,
          end_use_at: product_ad.end_use_at,
          status: 0,
          product_type: ProductAd.product_types[product_ad.product_type]
        )
      end
    end

    return false unless order.save
    order.reload
    order.aprovado! if order.product_ad_orders.pendente.blank?
    order
  end

  def all_used?
    product_ad_orders.nao_utilizado.blank?
  end

  def use_all
    product_ad_orders.nao_utilizado.each do |product_ad_order|
      product_ad_order.use_coupon
    end
  end

  def dont_use_all
    product_ad_orders.utilizado.each do |product_ad_order|
      product_ad_order.dont_use_coupon
    end
  end

  def approve_all
    product_ad_orders.pendente.each do |product_ad_order|
      product_ad_order.approve_coupon
    end
    aprovado!
  end

  def cancel_all
    product_ad_orders.oferta.each do |product_ad_order|
      product_ad_order.cancel_coupon
    end
    cancelado!
    OrderMailer.delay.order_cancelled(id)
  end
end
