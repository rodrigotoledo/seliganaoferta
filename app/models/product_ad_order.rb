class ProductAdOrder < ActiveRecord::Base
  enum status: [ :pendente, :aprovado, :cancelado ]
  enum product_type: [ :oferta, :vale ]
  enum used: [ :nao_utilizado, :utilizado ]
  belongs_to :order
  belongs_to :client
  belongs_to :product_ad
  has_one :company, through: :product_ad
  has_one :order_transaction, through: :order
  # transacoes individuais
  has_many :product_ad_order_transactions
  after_save :approve_if_vale
  after_save :generate_coupon, if: "aprovado? && coupon.blank?"
  before_save :zero_on_vale_attributes, if: "vale?"

  def self.expireds
    where("end_use_at < ?", Date.today).order_by_end_use
  end

  def self.actives
    where("end_use_at >= ?", Date.today)
  end

  def self.order_by_end_use
    order(:end_use_at)
  end

  def self.pending
    joins(:order_transaction).pendente.order('product_ad_orders.updated_at DESC')
  end

  def zero_on_vale_attributes
    real_price     = nil
    special_price  = nil
    discount_price = nil
    true
  end

  def approve_if_vale
    aprovado! if vale? && !aprovado?
    true
  end

  def generate_coupon
    new_coupon = nil
    begin
      new_coupon = SecureRandom.hex(6)
    end while ProductAdOrder.where(coupon: new_coupon).exists?
    update_attribute(:coupon, new_coupon)
    OrderMailer.delay.new_coupon(id)
  end

  def use_coupon
    utilizado!
    update_attribute(:used_at, Time.now)
    OrderMailer.delay.coupon_used(id)
  end

  def dont_use_coupon
    nao_utilizado!
    update_attribute(:used_at, nil)
  end

  def approve_coupon
    update_attribute(:coupon, nil) unless coupon.blank?
    aprovado!
  end

  def cancel_coupon
    cancelado!
    OrderMailer.delay.coupon_cancelled(id)
  end
end
