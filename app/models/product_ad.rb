class ProductAd < ActiveRecord::Base
  extend FriendlyId
  enum product_type: [ :oferta, :vale ]
  belongs_to :product_ad
  belongs_to :company
  has_one :city, through: :company
  has_many :product_ad_images
  has_many :product_ad_orders
  has_many :product_ads

  friendly_id :title, use: :slugged

  has_attached_file :image, styles: {thumb: '76x76#', big: '600x600#',
    box: '215x215#', master: '770x372#', small: '370x178#'},
    convert_options: {big: "-quality 90 -strip" }
  do_not_validate_attachment_file_type :image

  def self.actives
    joins(:company).where(active: true).where("? BETWEEN product_ads.start_at AND product_ads.end_at", Date.today)
  end

  def self.only_masters
    where(product_ad_id: nil)
  end

  def self.expireds
    joins(:company).where(active: true).where("product_ads.end_at < ?", Date.today)
  end

  def total_coupons
    @total_coupons ||= product_ad_orders.count
  end

  def touch_views
    update_attribute(:views, views+1)
  end

  def newer?(to = :client)
    if to == :client
      start_at >= Date.today.weeks_ago(1)
    else
      start_use_at >= Date.today.weeks_ago(1)
    end
  end

  def ending?(to = :client)
    if to == :client
      end_at <= Date.today.days_since(2) && end_at >= Date.today
    else
      end_use_at <= Date.today.days_since(2) && end_use_at >= Date.today
    end
  end

  def expired?
    end_use_at < Date.today
  end

  def can_be_sold?
    active && Date.today >= start_at && Date.today <= end_at
  end

  def discount
    return 100 if vale?

    (100.0 - ((special_price * 100) / real_price.to_f)).round(2)
  end

  def url
    to_param
  end
end
