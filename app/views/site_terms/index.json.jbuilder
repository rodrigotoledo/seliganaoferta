json.array!(@site_terms) do |site_term|
  json.extract! site_term, :id, :terms
  json.url site_term_url(site_term, format: :json)
end
