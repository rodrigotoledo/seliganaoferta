json.array!(@admin_clients) do |admin_client|
  json.extract! admin_client, :id, :email, :name, :phone, :mobile, :cpf, :rg, :address, :address_city, :address_state, :address_neighborhood, :address_zipcode, :newsletter, :agree, :city_id, :company_id, :site_term_id
  json.url admin_client_url(admin_client, format: :json)
end
