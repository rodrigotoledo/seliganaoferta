json.array!(@companies) do |company|
  json.extract! company, :id, :city_id, :name, :document, :email, :address, :contact_name, :contact_phone, :contact_mobile, :state
  json.url company_url(company, format: :json)
end
