json.array!(@orders) do |order|
  json.extract! order, :id, :client_id, :status, :payment_qty, :total, :amount_by_qty, :transaction_code
  json.url order_url(order, format: :json)
end
