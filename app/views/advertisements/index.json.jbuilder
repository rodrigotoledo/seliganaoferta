json.array!(@advertisements) do |advertisement|
  json.extract! advertisement, :id, :city_id, :title, :url, :description, :html_description, :start_at, :end_at, :master, :small, :views
  json.url advertisement_url(advertisement, format: :json)
end
