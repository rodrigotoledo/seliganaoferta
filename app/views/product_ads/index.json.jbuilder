json.array!(@product_ads) do |product_ad|
  json.extract! product_ad, :id, :company_id, :title, :description, :terms, :start_at, :end_at, :start_use_at, :end_use_at, :real_price, :special_price, :minimum_per_client, :maximum_per_client, :views, :product_type, :is_hostel, :use_in_holiday, :active
  json.url product_ad_url(product_ad, format: :json)
end
