json.array!(@product_ad_images) do |product_ad_image|
  json.extract! product_ad_image, :id, :product_ad_id
  json.url product_ad_image_url(product_ad_image, format: :json)
end
