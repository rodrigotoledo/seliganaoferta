json.array!(@product_ad_order_transactions) do |product_ad_order_transaction|
  json.extract! product_ad_order_transaction, :id, :product_ad_order_id, :order_transaction_id, :gross_amount, :discount_amount, :fee_amount, :net_amount, :extra_amount, :reference_id
  json.url product_ad_order_transaction_url(product_ad_order_transaction, format: :json)
end
