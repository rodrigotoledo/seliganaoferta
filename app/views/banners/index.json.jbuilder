json.array!(@banners) do |banner|
  json.extract! banner, :id, :title, :url, :start_at, :end_at, :active, :views
  json.url banner_url(banner, format: :json)
end
