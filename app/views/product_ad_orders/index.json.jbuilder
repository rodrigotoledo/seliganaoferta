json.array!(@product_ad_orders) do |product_ad_order|
  json.extract! product_ad_order, :id, :order_id, :client_id, :product_ad_id, :real_price, :special_price, :discount, :status, :annotations, :coupon, :terms, :start_at, :end_at, :start_use_at, :end_use_at, :product_type
  json.url product_ad_order_url(product_ad_order, format: :json)
end
