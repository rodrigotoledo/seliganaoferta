class ContactMailer < ActionMailer::Base
  layout 'email'
  default from: "no-reply@rotaempresarial.com.br"
  helper :application

  def new_contact(mail_attributes)
    @name    = mail_attributes[:name]
    @subject = mail_attributes[:subject]
    @email   = mail_attributes[:email]
    @message = mail_attributes[:message]
    @company = mail_attributes[:company]

    mail to: "contato@rotaempresarial.com.br", bcc: Admin::ADMIN_EMAILS, reply_to: @email
  end
end
