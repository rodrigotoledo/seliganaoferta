class OrderMailer < ActionMailer::Base
  layout 'email'
  default from: "no-reply@rotaempresarial.com.br"
  helper :application

  def new_order(order_id)
    @order = Order.find(order_id)

    mail to: @order.client.email, bcc: Admin::ADMIN_EMAILS
  end

  def order_cancelled(order_id)
    @order = Order.find(order_id)

    mail to: @order.client.email, bcc: Admin::ADMIN_EMAILS
  end

  def new_coupon(product_ad_order_id)
    @product_ad_order = ProductAdOrder.find(product_ad_order_id)
    bcc_emails = Admin::ADMIN_EMAILS
    bcc_emails << @product_ad_order.product_ad.company.email
    mail to: @product_ad_order.order.client.email, bcc: bcc_emails.compact
  end

  def coupon_used(product_ad_order_id)
    @product_ad_order = ProductAdOrder.find(product_ad_order_id)
    bcc_emails = Admin::ADMIN_EMAILS
    bcc_emails << @product_ad_order.product_ad.company.email
    mail to: @product_ad_order.order.client.email, bcc: bcc_emails.compact
  end

  def coupon_cancelled(product_ad_order_id)
    @product_ad_order = ProductAdOrder.find(product_ad_order_id)
    bcc_emails = Admin::ADMIN_EMAILS
    bcc_emails << @product_ad_order.product_ad.company.email
    mail to: @product_ad_order.order.client.email, cc: bcc_emails.compact,
      bcc: bcc_emails.compact
  end
end
