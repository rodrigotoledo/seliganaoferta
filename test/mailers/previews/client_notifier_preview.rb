# Preview all emails at http://localhost:3000/rails/mailers/client_notifier
class ClientNotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/client_notifier/registration
  def registration
    ClientNotifier.registration
  end

end
