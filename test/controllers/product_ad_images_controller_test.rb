require 'test_helper'

class ProductAdImagesControllerTest < ActionController::TestCase
  setup do
    @product_ad_image = product_ad_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_ad_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_ad_image" do
    assert_difference('ProductAdImage.count') do
      post :create, product_ad_image: { product_ad_id: @product_ad_image.product_ad_id }
    end

    assert_redirected_to product_ad_image_path(assigns(:product_ad_image))
  end

  test "should show product_ad_image" do
    get :show, id: @product_ad_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_ad_image
    assert_response :success
  end

  test "should update product_ad_image" do
    patch :update, id: @product_ad_image, product_ad_image: { product_ad_id: @product_ad_image.product_ad_id }
    assert_redirected_to product_ad_image_path(assigns(:product_ad_image))
  end

  test "should destroy product_ad_image" do
    assert_difference('ProductAdImage.count', -1) do
      delete :destroy, id: @product_ad_image
    end

    assert_redirected_to product_ad_images_path
  end
end
