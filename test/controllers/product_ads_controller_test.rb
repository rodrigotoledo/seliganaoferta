require 'test_helper'

class ProductAdsControllerTest < ActionController::TestCase
  setup do
    @product_ad = product_ads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_ads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_ad" do
    assert_difference('ProductAd.count') do
      post :create, product_ad: { active: @product_ad.active, company_id: @product_ad.company_id, description: @product_ad.description, end_at: @product_ad.end_at, end_use_at: @product_ad.end_use_at, is_hostel: @product_ad.is_hostel, maximum_per_client: @product_ad.maximum_per_client, minimum_per_client: @product_ad.minimum_per_client, product_type: @product_ad.product_type, real_price: @product_ad.real_price, special_price: @product_ad.special_price, start_at: @product_ad.start_at, start_use_at: @product_ad.start_use_at, terms: @product_ad.terms, title: @product_ad.title, use_in_holiday: @product_ad.use_in_holiday, views: @product_ad.views }
    end

    assert_redirected_to product_ad_path(assigns(:product_ad))
  end

  test "should show product_ad" do
    get :show, id: @product_ad
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_ad
    assert_response :success
  end

  test "should update product_ad" do
    patch :update, id: @product_ad, product_ad: { active: @product_ad.active, company_id: @product_ad.company_id, description: @product_ad.description, end_at: @product_ad.end_at, end_use_at: @product_ad.end_use_at, is_hostel: @product_ad.is_hostel, maximum_per_client: @product_ad.maximum_per_client, minimum_per_client: @product_ad.minimum_per_client, product_type: @product_ad.product_type, real_price: @product_ad.real_price, special_price: @product_ad.special_price, start_at: @product_ad.start_at, start_use_at: @product_ad.start_use_at, terms: @product_ad.terms, title: @product_ad.title, use_in_holiday: @product_ad.use_in_holiday, views: @product_ad.views }
    assert_redirected_to product_ad_path(assigns(:product_ad))
  end

  test "should destroy product_ad" do
    assert_difference('ProductAd.count', -1) do
      delete :destroy, id: @product_ad
    end

    assert_redirected_to product_ads_path
  end
end
