require 'test_helper'

class OfertasControllerTest < ActionController::TestCase
  test "should get detalhes" do
    get :detalhes
    assert_response :success
  end

  test "should get disponiveis" do
    get :disponiveis
    assert_response :success
  end

  test "should get encerradas" do
    get :encerradas
    assert_response :success
  end

end
