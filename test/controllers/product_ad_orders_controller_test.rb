require 'test_helper'

class ProductAdOrdersControllerTest < ActionController::TestCase
  setup do
    @product_ad_order = product_ad_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_ad_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_ad_order" do
    assert_difference('ProductAdOrder.count') do
      post :create, product_ad_order: { annotations: @product_ad_order.annotations, client_id: @product_ad_order.client_id, coupon: @product_ad_order.coupon, discount: @product_ad_order.discount, end_at: @product_ad_order.end_at, end_use_at: @product_ad_order.end_use_at, order_id: @product_ad_order.order_id, product_ad_id: @product_ad_order.product_ad_id, product_type: @product_ad_order.product_type, real_price: @product_ad_order.real_price, special_price: @product_ad_order.special_price, start_at: @product_ad_order.start_at, start_use_at: @product_ad_order.start_use_at, status: @product_ad_order.status, terms: @product_ad_order.terms }
    end

    assert_redirected_to product_ad_order_path(assigns(:product_ad_order))
  end

  test "should show product_ad_order" do
    get :show, id: @product_ad_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_ad_order
    assert_response :success
  end

  test "should update product_ad_order" do
    patch :update, id: @product_ad_order, product_ad_order: { annotations: @product_ad_order.annotations, client_id: @product_ad_order.client_id, coupon: @product_ad_order.coupon, discount: @product_ad_order.discount, end_at: @product_ad_order.end_at, end_use_at: @product_ad_order.end_use_at, order_id: @product_ad_order.order_id, product_ad_id: @product_ad_order.product_ad_id, product_type: @product_ad_order.product_type, real_price: @product_ad_order.real_price, special_price: @product_ad_order.special_price, start_at: @product_ad_order.start_at, start_use_at: @product_ad_order.start_use_at, status: @product_ad_order.status, terms: @product_ad_order.terms }
    assert_redirected_to product_ad_order_path(assigns(:product_ad_order))
  end

  test "should destroy product_ad_order" do
    assert_difference('ProductAdOrder.count', -1) do
      delete :destroy, id: @product_ad_order
    end

    assert_redirected_to product_ad_orders_path
  end
end
