require 'test_helper'

class SiteTermsControllerTest < ActionController::TestCase
  setup do
    @site_term = site_terms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:site_terms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create site_term" do
    assert_difference('SiteTerm.count') do
      post :create, site_term: { terms: @site_term.terms }
    end

    assert_redirected_to site_term_path(assigns(:site_term))
  end

  test "should show site_term" do
    get :show, id: @site_term
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @site_term
    assert_response :success
  end

  test "should update site_term" do
    patch :update, id: @site_term, site_term: { terms: @site_term.terms }
    assert_redirected_to site_term_path(assigns(:site_term))
  end

  test "should destroy site_term" do
    assert_difference('SiteTerm.count', -1) do
      delete :destroy, id: @site_term
    end

    assert_redirected_to site_terms_path
  end
end
