require 'test_helper'

class ProductAdOrderTransactionsControllerTest < ActionController::TestCase
  setup do
    @product_ad_order_transaction = product_ad_order_transactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_ad_order_transactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_ad_order_transaction" do
    assert_difference('ProductAdOrderTransaction.count') do
      post :create, product_ad_order_transaction: { discount_amount: @product_ad_order_transaction.discount_amount, extra_amount: @product_ad_order_transaction.extra_amount, fee_amount: @product_ad_order_transaction.fee_amount, gross_amount: @product_ad_order_transaction.gross_amount, net_amount: @product_ad_order_transaction.net_amount, order_transaction_id: @product_ad_order_transaction.order_transaction_id, product_ad_order_id: @product_ad_order_transaction.product_ad_order_id, reference_id: @product_ad_order_transaction.reference_id }
    end

    assert_redirected_to product_ad_order_transaction_path(assigns(:product_ad_order_transaction))
  end

  test "should show product_ad_order_transaction" do
    get :show, id: @product_ad_order_transaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_ad_order_transaction
    assert_response :success
  end

  test "should update product_ad_order_transaction" do
    patch :update, id: @product_ad_order_transaction, product_ad_order_transaction: { discount_amount: @product_ad_order_transaction.discount_amount, extra_amount: @product_ad_order_transaction.extra_amount, fee_amount: @product_ad_order_transaction.fee_amount, gross_amount: @product_ad_order_transaction.gross_amount, net_amount: @product_ad_order_transaction.net_amount, order_transaction_id: @product_ad_order_transaction.order_transaction_id, product_ad_order_id: @product_ad_order_transaction.product_ad_order_id, reference_id: @product_ad_order_transaction.reference_id }
    assert_redirected_to product_ad_order_transaction_path(assigns(:product_ad_order_transaction))
  end

  test "should destroy product_ad_order_transaction" do
    assert_difference('ProductAdOrderTransaction.count', -1) do
      delete :destroy, id: @product_ad_order_transaction
    end

    assert_redirected_to product_ad_order_transactions_path
  end
end
