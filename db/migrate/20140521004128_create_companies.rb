class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.references :city, index: true
      t.string :name
      t.string :document
      t.string :email
      t.text :address
      t.string :contact_name
      t.string :contact_phone
      t.string :contact_mobile
      t.string :state

      t.timestamps
    end
  end
end
