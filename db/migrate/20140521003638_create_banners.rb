class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.string :url
      t.attachment :image
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :active
      t.integer :views

      t.timestamps
    end
  end
end
