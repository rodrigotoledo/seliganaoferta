class CreateProductAdOrders < ActiveRecord::Migration
  def change
    create_table :product_ad_orders do |t|
      t.references :order, index: true
      t.references :client, index: true
      t.references :product_ad, index: true
      t.float :real_price
      t.float :special_price
      t.float :discount
      t.integer :status
      t.text :annotations
      t.string :coupon
      t.text :terms
      t.date :start_at
      t.date :end_at
      t.date :start_use_at
      t.date :end_use_at
      t.integer :product_type

      t.timestamps
    end
  end
end
