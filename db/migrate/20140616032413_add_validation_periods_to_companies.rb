class AddValidationPeriodsToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :active, :boolean, default: true
    add_column :companies, :start_at, :date, default: '2014-01-01'
    add_column :companies, :end_at, :date, default: '2015-01-01'
  end
end
