class AddSellPriceAndSoldByToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :sell_price, :float
    add_column :companies, :sold_by, :string
  end
end
