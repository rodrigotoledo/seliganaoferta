class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.references :city, index: true
      t.string :title
      t.string :url
      t.attachment :image
      t.text :description
      t.text :html_description
      t.datetime :start_at
      t.datetime :end_at
      t.boolean :master
      t.boolean :small
      t.boolean :active
      t.integer :views

      t.timestamps
    end
  end
end
