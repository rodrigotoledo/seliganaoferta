class CreateProductAds < ActiveRecord::Migration
  def change
    create_table :product_ads do |t|
      t.references :company, index: true
      t.string :title
      t.attachment :image
      t.text :description
      t.text :terms
      t.date :start_at
      t.date :end_at
      t.date :start_use_at
      t.date :end_use_at
      t.float :real_price
      t.float :special_price
      t.integer :minimum_per_client, default: 1
      t.integer :maximum_per_client, default: 5
      t.integer :views, default: 0
      t.integer :product_type, default: 0
      t.boolean :is_hostel, default: false
      t.boolean :use_in_holiday, default: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
