class RenameCompanyPhoto < ActiveRecord::Migration
  def change
    rename_column :companies, :photo_file_name, :photo0_file_name
    rename_column :companies, :photo_content_type, :photo0_content_type
    rename_column :companies, :photo_file_size, :photo0_file_size
    rename_column :companies, :photo_updated_at, :photo0_updated_at
  end
end
