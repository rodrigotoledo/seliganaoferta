class AddLogoAndPhotoToCompanies < ActiveRecord::Migration
  def up
    change_table :companies do |t|
      t.attachment :logo
      t.attachment :photo
    end
  end
end
