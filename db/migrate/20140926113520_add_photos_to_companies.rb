class AddPhotosToCompanies < ActiveRecord::Migration
  def up
    change_table :companies do |t|
      t.attachment :photo1
      t.attachment :photo2
      t.attachment :photo3
      t.attachment :photo4
      t.attachment :photo5
    end
  end
end
