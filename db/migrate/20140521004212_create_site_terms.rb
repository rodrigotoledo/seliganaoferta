class CreateSiteTerms < ActiveRecord::Migration
  def change
    create_table :site_terms do |t|
      t.text :terms

      t.timestamps
    end
  end
end
