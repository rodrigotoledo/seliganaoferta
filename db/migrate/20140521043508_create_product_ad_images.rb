class CreateProductAdImages < ActiveRecord::Migration
  def change
    create_table :product_ad_images do |t|
      t.references :product_ad, index: true
      t.attachment :image

      t.timestamps
    end
  end
end
