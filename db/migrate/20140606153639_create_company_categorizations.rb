class CreateCompanyCategorizations < ActiveRecord::Migration
  def change
    create_table :company_categorizations do |t|
      t.references :company, index: true
      t.references :company_category, index: true

      t.timestamps
    end
  end
end
