class AddTransferPriceToProductAds < ActiveRecord::Migration
  def change
    add_column :product_ads, :transfer_price, :float
    add_column :product_ad_orders, :transfer_price, :float
  end
end
