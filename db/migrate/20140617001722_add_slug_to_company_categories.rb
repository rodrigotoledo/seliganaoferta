class AddSlugToCompanyCategories < ActiveRecord::Migration
  def change
    add_column :company_categories, :slug, :string
  end
end
