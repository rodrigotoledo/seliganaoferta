class CreateOrderTransactions < ActiveRecord::Migration
  def change
    create_table :order_transactions do |t|
      t.references :order, index: true
      t.text :full_response
      t.string :payment_status
      t.string :payment_method
      t.float :gross_amount
      t.float :discount_amount
      t.float :fee_amount
      t.float :net_amount
      t.float :extra_amount
      t.string :transaction_id
      t.string :code

      t.timestamps
    end
  end
end
