class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :client, index: true
      t.integer :status
      t.integer :payment_qty
      t.float :total
      t.integer :amount_by_qty
      t.text :transaction_code

      t.timestamps
    end
  end
end
