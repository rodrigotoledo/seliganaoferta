class CreateAccessLogs < ActiveRecord::Migration
  def change
    create_table :access_logs do |t|
      t.text :url
      t.text :log
      t.references :client, index: true
      t.string :ip

      t.timestamps
    end
  end
end
