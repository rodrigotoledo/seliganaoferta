class AddUsedAndUsedAtToProductAdOrders < ActiveRecord::Migration
  def change
    add_column :product_ad_orders, :used, :integer, default: 0
    add_column :product_ad_orders, :used_at, :datetime
  end
end
