class CreateProductAdOrderTransactions < ActiveRecord::Migration
  def change
    create_table :product_ad_order_transactions do |t|
      t.references :product_ad_order, index: true
      t.references :order_transaction, index: true
      t.references :product_ad_order, index: true
      t.float :gross_amount
      t.float :discount_amount
      t.float :fee_amount
      t.float :net_amount
      t.float :extra_amount

      t.timestamps
    end
  end
end
