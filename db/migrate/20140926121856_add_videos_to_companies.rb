class AddVideosToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :video0
      t.string :video1
      t.string :video2
      t.string :video3
    end
  end
end
