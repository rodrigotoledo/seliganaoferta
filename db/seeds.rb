# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
City.create([
  { name: 'Caratinga' },
  { name: 'Raul Soares' },
  # { name: 'Ponte Nova' },
  { name: 'Inhapim' }
  ])
Company.create(city_id: City.first.id, name: 'Agencia Invent')
Page.create([
  { title: 'Sobre a Empresa', link: 'sobre-a-empresa' },
  { title: 'Anuncie Conosco', link: 'anuncie-conosco' }
  ])
SiteTerm.create(terms: '...')

Admin.create(email: "rodrigo.toledo@rotaempresarial.com.br", password: "hahire45", password_confirmation: "hahire45")
Client.create(email: "rodrigo.toledo@rotaempresarial.com.br", name: 'Rodrigo Toledo',
  password: "hahire45", password_confirmation: "hahire45",
  city: City.first, company: Company.first, site_term: SiteTerm.first)

Admin.create(email: "rafael.alves@rotaempresarial.com.br", password: "rota123", password_confirmation: "rota123")
Client.create(email: "rafael.alves@rotaempresarial.com.br", name: 'Rodrigo Toledo',
  password: "rota123", password_confirmation: "rota123",
  city: City.first, company: Company.first, site_term: SiteTerm.first)

# images = ['a_ai_04.jpg', 'a_bt_01.jpg', 'a_bt_02.jpg']
# 12.times do |i|
#   real_price = (i+1)*rand(999)
#   special_price = real_price - real_price*0.3
#   end_at = Date.today+(rand(120)+1).days
#   product_ad = ProductAd.create(title: "Oferta #{i}",
#     image: File.open(File.join(::Rails.root,'db','seed_images',images.sample)),
#     start_at: Date.yesterday,
#     end_at: end_at,
#     start_use_at: Date.yesterday,
#     end_use_at: end_at,
#     real_price: real_price,
#     special_price: special_price,
#     active: true,
#     company: Company.first)
#   images.each do |basename|
#     product_ad_image = product_ad.product_ad_images.build(image: File.open(File.join(::Rails.root,'db','seed_images',basename)))
#     product_ad_image.save
#   end
# end

# 4.times do |i|
#   real_price = (i+1)*rand(999)
#   special_price = real_price - real_price*0.3
#   end_at = Date.today+(rand(120)+1).days
#   product_ad = ProductAd.create(title: "Oferta #{i}",
#     image: File.open(File.join(::Rails.root,'db','seed_images',images.sample)),
#     start_at: Date.yesterday,
#     end_at: end_at,
#     start_use_at: Date.yesterday,
#     end_use_at: end_at,
#     real_price: real_price,
#     special_price: special_price,
#     active: true,
#     company: Company.first)
#   product_ad.vale!
#   images.each do |basename|
#     product_ad_image = product_ad.product_ad_images.build(image: File.open(File.join(::Rails.root,'db','seed_images',basename)))
#     product_ad_image.save
#   end
# end

# 8.times do |i|
#   real_price = (i+1)*rand(999)
#   special_price = real_price - real_price*0.3
#   start_at = Date.today.years_ago(1)
#   end_at = start_at + 3.months
#   product_ad = ProductAd.create(title: "Oferta #{i}",
#     image: File.open(File.join(::Rails.root,'db','seed_images',images.sample)),
#     start_at: start_at,
#     end_at: end_at,
#     start_use_at: start_at,
#     end_use_at: end_at,
#     real_price: real_price,
#     special_price: special_price,
#     active: true,
#     company: Company.first)
#   images.each do |basename|
#     product_ad_image = product_ad.product_ad_images.build(image: File.open(File.join(::Rails.root,'db','seed_images',basename)))
#     product_ad_image.save
#   end
# end

Advertisement.create(image: File.open(File.join(::Rails.root,'db','seed_images','Sexy-Santa-Girls-16.jpg')),
  city: City.first,
  title: 'Filo',
  description: '...',
  url: 'http://www.filolindamulher.com.br',
  start_at: Date.yesterday,
  end_at: (Date.today+1.year),
  active: true,
  master: true
  )

Advertisement.create(image: File.open(File.join(::Rails.root,'db','seed_images','aula.jpg')),
  city: City.first,
  title: 'Aula Particular Biologia',
  html_description: '<div class="background_white"><h4>Biologia<span>aprenda de forma rápida</span></h4></div>',
  url: 'http://www.filolindamulher.com.br',
  start_at: Date.yesterday,
  end_at: (Date.today+1.year),
  active: true,
  small: true
  )

Advertisement.create(image: File.open(File.join(::Rails.root,'db','seed_images','steak.jpeg')),
  city: City.first,
  title: 'Espeto Grill',
  html_description: '<div class="background_white"><h4>A melhor carne<span>agora com descontos especiais</span></h4></div>',
  url: 'http://www.filolindamulher.com.br',
  start_at: Date.yesterday,
  end_at: (Date.today+1.year),
  active: true,
  small: true
  )
