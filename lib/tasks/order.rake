namespace :order do
  desc "Delete invalid orders"
  task :delete_invalid => :environment do
    Order.pendente.where('created_at <= ?', Time.now-72.hours).each do |order|
      next if order.order_transaction.exists?
      order.destroy_all
    end
  end
end
