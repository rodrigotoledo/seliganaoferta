namespace :order_transaction do
  desc "Update pending orders"
  task :update_pending => :environment do
    OrderTransaction.update_pending!
  end
end
