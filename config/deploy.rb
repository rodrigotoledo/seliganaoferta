lock '3.2.1'

set :application, 'rotaempresarial'
set :repo_url, 'https://rtoledo:hahire45@bitbucket.org/rtoledo/seliganaoferta.git'
server "rtoledo.inf.br", user: 'rotaempresarial.com.br', port: 22, password: "EMh6A5hz85q2pV", roles: %w{web app db}
set :rvm_type, :user
set :rvm_ruby_version, '2.0.0@rotaempresarial'
# set :default_env, { rvm_bin_path: '~/.rvm/bin' }
# SSHKit.config.command_map[:rake] = "#{fetch(:default_env)[:rvm_bin_path]}/rvm ruby-#{fetch(:rvm_ruby_version)} do bundle exec rake"

set :branch, "master"
set :format, :pretty
set :use_sudo, false
set :keep_releases, 5
set :bundle_gemfile, -> { release_path.join('Gemfile') }
set :passenger_restart_with_touch, true
# set :unicorn_pid, -> { "/home/seliganaoferta.com.br/pids/unicorn.pid" }

# set :delayed_job_server_role, :worker
# set :delayed_job_args, "-n 2"

# after "deploy:stop",    "delayed_job:stop"
# after "deploy:start",   "delayed_job:start"
# after "deploy:restart", "delayed_job:restart"

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# namespace :deploy do

#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       # Your restart mechanism here, for example:
#       # execute :touch, release_path.join('tmp/restart.txt')
#     end
#   end

#   after :publishing, :restart

#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end

# end
