Rails.application.routes.draw do
  resources :company_categories
  resources :admin_clients

  resources :order_transactions do
    collection do
      get :lost
    end
  end

  resources :product_ad_orders

  resources :orders do
    collection do
      get :update_pending, :approved, :pending, :cancelled
    end
    member do
      get 'approve'
      get 'cancel'
      get 'use_all'
      get 'dont_use_all'
      get 'use_coupon'
      get 'dont_use_coupon'
      get 'approve_coupon'
      get 'cancel_coupon'
    end
  end

  resources :product_ad_images

  resources :product_ads do
    member do
      get :duplicate
    end
  end

  resources :site_terms

  resources :pages

  resources :companies do
    collection do
      get :inactives
    end
  end

  resources :advertisements

  resources :cities

  resources :banners

  resources :holidays

  devise_for :clients, path: 'usuarios', path_names: { sign_in: 'entrar', sign_out: 'sair', registration: 'registro', sign_up: 'criar-conta', edit: 'meus-dados', password: 'senha' }
  devise_for :admins
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pagina_inicial#index'

  # Example of regular route:
  get 'guia-empresarial' => 'guia_empresarial#index', as: :guia_empresarial
  get 'empresa/:id' => 'guia_empresarial#empresa', as: :empresa
  get 'area-atuacao/empresas/:id' => 'guia_empresarial#empresas_por_area_atuacao', as: :empresas_por_area_atuacao
  get 'ofertas/detalhes/:id' => 'ofertas#detalhes', as: :ofertas_detalhes
  get 'ofertas-disponiveis' => 'ofertas#disponiveis', as: :ofertas_disponiveis
  get 'ofertas-encerradas' => 'ofertas#encerradas', as: :ofertas_encerradas
  get 'minha-conta' => 'minha_conta#index', as: :minha_conta
  get 'minha-conta/validar-email' => 'minha_conta#validar_email', as: :validar_email
  get 'minha-conta/validar-cpf' => 'minha_conta#validar_cpf', as: :validar_cpf
  post 'minha-conta/notificacoes-compra' => 'minha_conta#notificacoes_compra', as: :notificacoes_compra, via: [:post]
  get 'regras-gerais' => 'paginas#regras_gerais', as: :regras_gerais
  get 'cupons' => 'minha_conta#cupons', as: :cupons
  get 'resultado-busca' => 'busca#index', as: :resultado_busca
  get 'cidades' => 'cidades#index', as: :cidades
  get 'trocar-cidade/:id' => 'cidades#trocar', as: :trocar_cidade
  get 'paginas/:link' => 'paginas#index', as: :pagina

  get 'carrinho' => 'carrinho#index', as: :carrinho
  get 'carrinho/adicionar/:id' => 'carrinho#adicionar', as: :carrinho_adicionar, via: [:get, :post]
  get 'carrinho/diminuir/:id' => 'carrinho#diminuir', as: :carrinho_diminuir
  get 'carrinho/remover/:id' => 'carrinho#remover', as: :carrinho_remover
  get 'carrinho/pagamento' => 'carrinho#pagamento', as: :carrinho_pagamento
  post 'carrinho/efetuar-pagamento' => 'carrinho#efetuar_pagamento', as: :carrinho_efetuar_pagamento
  get 'carrinho/processando' => 'carrinho#processando', as: :carrinho_processando

  get 'contato' => 'contato#index', as: :contato
  post 'contato/enviar' => 'contato#enviar', as: :contato_enviar_mensagem

  get 'painel-controle' => 'painel_controle#index', as: :painel_controle
  get 'painel-controle/administrar-oferta/:id' => 'painel_controle#administrar_oferta', as: :painel_controle_administrar_oferta
  get 'painel-controle/utilizar-cupon/:id' => 'painel_controle#utilizar_cupon', as: :painel_controle_utilizar_cupon
  get 'painel-controle/cupons-ativos' => 'painel_controle#cupons_ativos', as: :painel_controle_cupons_ativos
end
